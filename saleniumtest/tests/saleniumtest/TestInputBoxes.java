package saleniumtest;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestInputBoxes {

	@Before
	public void setUp() throws Exception {
		//1 setup selenium + web driver
		System.setProperty("webdriver.chrome.driver", "/Users/macstudent/Desktop/chromedriver");
		WebDriver driver = new ChromeDriver();
		// 2. go to the link
		driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
		
		
	}

	@After
	public void tearDown() throws Exception {
	}

	
	
	
	//TC1
	@Test
	public void testSingleInputField() throws InterruptedException {
		//1 ERNter some value onto thew text box
		//2push the bitton
		//3 get the op on the screen
		// check if the actul is expected
		
				//3. write the code to do stuff on the website
				WebElement inputBox = driver.findElement(By.id("user-message"));
				inputBox.sendKeys("Bla Bla Bka");
				
				
				// another way
				
				WebElement showMessageButton = driver.findElement(By.cssSelector("form#get-input button"));
				showMessageButton.click();
			WebElement outputBox = driver.findElement(By.id("display"));
			String actualOutput = outputBox.getText();
			assertEquals("Bla Bla Bka", actualOutput);
//				//4. close the browser
//				Thread.sleep(50000);  //pause for 1 second before closing the browser
//				driver.close();
	}
//TC2
	@Test
	public void testTwoInputField() {
		//1 setup selenium + web driver
		System.setProperty("webdriver.chrome.driver", "/Users/macstudent/Desktop/chromedriver");
		WebDriver driver = new ChromeDriver();
		// 2. go to the link
		driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
		//3. write the code to do stuff on the website
		WebElement inputBox1 = driver.findElement(By.id("sum1"));
		inputBox1.sendKeys("50");
		
		
		//3. write the code to do stuff on the website
		WebElement inputBox2 = driver.findElement(By.id("sum2"));
		inputBox2.sendKeys("25");
		
		
		//pres a button
		WebElement button = driver.findElement(By.cssSelector("form#gettotal button"));
		button.click();
		
		//4. get the output on the screen
		WebElement outputBox = driver.findElement(By.id("displayvalue"));
		String actualOutput = outputBox.getText();
		
		//5. check if actuL == EXPECTED
		assertEquals("75", actualOutput);
		
		
		
	}
}
