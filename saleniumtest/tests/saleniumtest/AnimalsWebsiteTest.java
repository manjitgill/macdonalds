package saleniumtest;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AnimalsWebsiteTest {
	//Coinfiguration variables:----
	//---------------------------------------
	// Location of chrome driver
	final String CHROMEDRIVER_LOCATION =  "/Users/macstudent/Desktop/chromedriver";
	final String URL_TO_TEST = "https://www.webdirectory.com/Animals";
	WebDriver driver;
	@Before
	public void setUp() throws Exception {
		//1 setup selenium + web driver
		System.setProperty("webdriver.chrome.driver",CHROMEDRIVER_LOCATION);
		 driver = new ChromeDriver();
		// 2. go to the link
		driver.get(URL_TO_TEST);
		
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testNumberOfLinks() {
		// check if the no. of links is equal to 10
		// 1.get the bulleted links in the section
		//List<WebElement> bulettedLinks = driver.findElements(By.cssSelector("table+ul li a"));
		// Solution number two
		// get all the <ul>
		List<WebElement> uls = driver.findElements(By.cssSelector("ul"));
		WebElement firstUL = uls.get(0);
		List<WebElement> bulletLinks = firstUL.findElements(By.cssSelector("li a"));
		
		
		System.out.print("No of links in the list are " + bulletLinks.size());
		for (int i = 0; i < bulletLinks.size(); i++) {
			WebElement link = bulletLinks.get(i);
		//	get the link text
			String url = link.getText();
			//	 get the link
			String linkUrl = link.getAttribute("href");
			// output
			System.out.print("  Link Text + " + linkUrl);
			System.out.print(url);
		}
		// 2. count the links
		
		
		// 3. check if its 10
		int noOfLinks = bulletLinks.size();
		assertEquals(10, noOfLinks);
		
	
	}

}
